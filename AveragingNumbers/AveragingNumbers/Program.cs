﻿// See https://aka.ms/new-console-template for more information
// Console.WriteLine("Hello, World!");

using System;
namespace AveragingNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10;
            int b = 5;
            int c = 25;
            int d = 30;
            int numOfNumbers = 4;

            double average = (a + b + c + d) / (double) numOfNumbers; 

            Console.WriteLine(average);
        }
    }
}

